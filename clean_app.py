import zipfile
import sys
import tempfile
import os
import shutil
import logging

logging.basicConfig(level=logging.DEBUG)
arch = sys.argv[1]
logging.debug(f'{arch} archive is added')

with zipfile.ZipFile(arch, 'r') as zip_ref:
    with tempfile.TemporaryDirectory() as tmpdir:
        logging.debug(f'Created temporary directory is {tmpdir}')
    zip_ref.extractall(tmpdir)

fl = 1
root = 0
dir = []
previous_path = ''
for dirpath, dirnames, files in os.walk(tmpdir):
    logging.debug(f'Found directory is a {dirpath}')
    if root <= 2:
        root_path = dirpath
        root += 1
        if root == 1:
            path_file = dirpath
    for file_name in files:
        logging.debug(f'with following files {file_name}')
        if file_name == "__init__.py":
            fl = 1
    if fl == 0 and root >= 1:
        shutil.rmtree(dirpath)
        dirpath = dirpath.replace(tmpdir, '')
        rm_path = dirpath[1:]
        logging.debug(f'{rm_path} directory will append to the file')
        dir.append(rm_path)
    fl = 0

file_path = path_file + '/cleaned.txt'

logging.debug(f'{file_path} was a file with list of\
deleted directory and amount of directory\
is {len(dir)}')

file = open(file_path, 'a')
if len(dir) != 0:
    dir.sort()
    for dir_str in dir:
        file.write(dir_str)
        file.write('\n')

file.close()

new_arch = arch.replace('.zip', '') + '_new'
logging.debug(f'{new_arch} archive was created from {root_path}')

shutil.make_archive(new_arch, 'zip', path_file)
